# Fork
https://bitbucket.org/ev45ive/huuuge-node2

git clone https://bitbucket.org/<user>/huuuge-node2
git remote add trener https://bitbucket.org/ev45ive/huuuge-node2
npm i

## Pobieranie 
git pull trener master -f 
git pull trener master --rebase 
<!-- git reset --hard trener/master -->

## Publikowanie twoich zmian
git commit -m "..."
<!-- git branch moje-zmiany-123 -->
git push origin/master -f


# Clone project
git clone https://bitbucket.org/ev45ive/huuuge-node2.git huuuge-node2
cd huuuge-node2
npm i
npm run watch

# Installations
node -v 
v14.5.0

npm -v 
6.14.5

code -v
1.40.1

git --version
git version 2.23.0.windows.1

# New project
npm init 
npm init -y

# NPM
https://semver.org/lang/pl/

npm audit // check
npm outdated
Package  Current  Wanted  Latest  Location
pupa       2.0.0   2.0.1   2.0.1  huuuge-node2

npm update  // install auto updates (wanted)

npm i // update semver deps and package-lock
npm ci // update from package-lock



# Mac/Ubuntu npm sudo 
.npmrc
NPM_PATH

# Typescript
npm i typescript --save-dev
npm i typescript --global
tsc --init

ts-node --project ./tsconfig.json ./src/index.ts

## TS definitions
npm i @types/node

https://www.typescriptlang.org/docs/handbook/declaration-files/dts-from-js.html

# Debugging
node --trace-uncaught

// tsconfig.json
<!-- "sourceMaps":true -->

node --enable-source-maps ./dist/index.js
Error: Upps...
    at Object.LogGen (C:\Projects\sages\huuuge-node2\dist\tasks\log-generator.js:5:11)
        -> C:\Projects\sages\huuuge-node2\src\tasks\log-generator.ts:4:9

node --inspect-brk ./dist/index.js
node --inspect ./dist/index.js


# TS-NODE for development
npm i ts-node -D
node  -r ts-node/register ./src/index.ts
node  --enable-source-maps -r ts-node/register ./src/index.ts
node --inspect-brk -r ts-node/register ./src/index.ts


# BUilding CLI apps
https://www.npmjs.com/package/minimist
https://www.npmjs.com/package/yargs


# Env / 12factor
cross-env
dotenv

# Middlewares
cors errorhandler morgan multer response-time timeout helmet
https://github.com/rajikaimal/awesome-express

# Upload
busboy
multer

# TDD/BDD Mocha + Chai
npm i -D chai mocha ts-node @types/chai @types/mocha


# ORM CLI
npm run typeorm -- entity:create -n product

npm run typeorm -- entity:create -n category

npm run typeorm -- schema:log

npm run typeorm -- migration:generate -n Add_Product_Entity

npm run typeorm -- migration:show

npm run typeorm -- migration:run

npm run typeorm -- migration:revert

# Deploy
npm install pm2 -g