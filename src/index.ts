import http from "http";
import express, { IRouterHandler, NextFunction, RequestHandler } from 'express'
import { userRoutes } from "./routes/users.routes";
import routes from "./routes";
import configMiddlewares from "./config";
import { configErrorHandlers } from "./config/errorhandlers";
import { createConnection, getConnection } from "typeorm";
// import dotenv from 'dotenv'
// dotenv.config({path})


const app = express()
configMiddlewares(app)

app.use('/static', express.static('./data/', {}))


app.use('/', routes);


app.use('/', (req, res) => {
  res.send('<h1>Hello Express</h1>');
});

configErrorHandlers(app)

const HOST = process.env.HOST || '127.0.0.1'
const PORT = process.env.PORT || '3000'

const connection = createConnection()

app.listen(parseInt(PORT), HOST, () => {
  console.log(`Listening on http://${HOST}:${PORT}/`)
  app.emit('ready')
})




