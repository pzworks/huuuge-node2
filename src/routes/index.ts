import { Router } from "express"
import { addressRoutes } from "./addresses.routes";
import { basketRoutes } from "./basket.routes";
import { orderRoutes } from "./orders.routes";
import { reviewRoutes } from "./reviews.routes";
import { userRoutes } from "./users.routes";
import { wishListRoutes } from "./wishlist.routes";
import { couponsRoutes } from "./coupons.routes";
import { categoryRoutes } from "./categories.routes"
import { paymentsRoutes } from "./payments.routes";
import { settingsRoutes } from "./settings.routes";
import { authRoutes } from "./auth.routes";
import { productsRoutes } from "./products.routes";

const routes = Router()
    .use('/auth', authRoutes)
    .use('/users', userRoutes)
    .use('/basket', basketRoutes)
    .use('/review', reviewRoutes)
    .use('/address', addressRoutes)
    .use('/orders', orderRoutes)
    .use('/payments', paymentsRoutes)
    .use('/wishlist', wishListRoutes)
    .use('/coupons', couponsRoutes)
    .use('/categories', categoryRoutes)
    .use('/settings',settingsRoutes)
    .use('/products',productsRoutes)

export default routes;