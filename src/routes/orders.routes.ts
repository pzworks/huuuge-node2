import { Router } from 'express';
import { ordersDTO } from '../interfaces/order.interfaces';

export const orderRoutes = Router();

orderRoutes.get<{}, ordersDTO, null, null>('/', (req, res) => {
    res.send(ordersData)
});

const ordersData:ordersDTO = {
    orders: [{
        id: 1,
        products: [],
    },
    {
        id: 2,
        products: [],
    }]
}